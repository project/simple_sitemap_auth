<?php

namespace Drupal\Tests\simple_sitemap_auth\Functional;

use Drupal\simple_sitemap\Queue\QueueWorker;
use Drupal\Tests\simple_sitemap\Functional\SimplesitemapTestBase;

/**
 * Tests Simple XML Sitemap functional integration.
 *
 * @group simple_sitemap_auth
 */
class SimplesitemapAuthTest extends SimplesitemapTestBase {

  /**
   * Modules to enable for this test.
   *
   * @var string[]
   */
  protected static $modules = [
    'simple_sitemap',
    'simple_sitemap_auth',
    'node',
    'media',
  ];

  /**
   * Auth user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * The auth sitemap URL.
   *
   * @var string
   */
  protected $authSitemapUrl = 'auth_hreflang/sitemap.xml';

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    // Normal user.
    $this->user = $this->drupalCreateUser([], NULL, FALSE);

    // Login as admin.
    $this->drupalLogin($this->privilegedUser);

    // Create media type.
    $this->drupalGet('/admin/structure/media/add');
    $this->submitForm([
      'label' => 'MediaImage',
      'id' => 'mediaimage',
      'source' => 'image',
    ], 'Save');
    $this->submitForm([
      'field_map[name]' => 'name',
    ], 'Save');
    $this->drupalGet('admin/config/media/media-settings');
    $this->submitForm([
      'standalone_url' => 1,
    ], 'Save');

    // Create a media item.
    $this->drupalGet('/media/add/mediaimage');
    $page = $this->getSession()->getPage();
    $page->fillField('Name', 'Foobar');
    $page->attachFileToField('Image', $this->root . '/core/modules/media/tests/fixtures/example_1.jpeg');
    $page->pressButton('Save');
    $page->fillField('Alt', 'Alttxt');
    $page->pressButton('Save');

    // Set sitemap user.
    $this->drupalGet('/admin/config/search/simplesitemap/user');
    $this->submitForm([
      'user' => $this->user->get('name')->value,
    ], 'Save configuration');

    // Set auth variant as default.
    $this->drupalGet('/admin/config/search/simplesitemap/variants/add');
    $this->submitForm([
      'label' => 'auth_hreflang',
      'id' => 'auth_hreflang',
      'type' => 'auth_hreflang',
      'description' => 'auth_hreflang',
    ], 'Save');

    // Set permissions.
    $this->drupalGet('/admin/people/permissions');
    $this->submitForm([
      'anonymous[view media]' => 0,
      'authenticated[view media]' => 1,
    ], 'Save permissions');
    $this->drupalGet('admin/reports/status/rebuild');
    $this->submitForm([], 'Rebuild permissions');

    // Set nodetype sitemap index.
    $this->drupalGet('/admin/structure/types/manage/page');
    $this->submitForm([
      'simple_sitemap[default][index]' => 1,
      'simple_sitemap[auth_hreflang][index]' => 1,
    ], 'Save');

    // Add entities to sitemap.
    $this->drupalGet('/admin/config/search/simplesitemap/entities');
    $this->submitForm([
      'entity_types[media]' => 1,
    ], 'Save');

    $this->drupalGet('/admin/config/search/simplesitemap/entities/media');
    $this->submitForm([
      'bundles[mediaimage][auth_hreflang][index]' => 1,
      'bundles[mediaimage][auth_hreflang][include_images]' => 1,
      'bundles[mediaimage][default][index]' => 1,
      'bundles[mediaimage][default][include_images]' => 1,
      'simple_sitemap_regenerate_now' => 1,
    ], 'Save');

    // Generate sitemaps.
    $this->generator->rebuildQueue()->generate(QueueWorker::GENERATE_TYPE_BACKEND);
  }

  /**
   * Tests Auth Sitemap.
   */
  public function testSitemapAuth() : void {
    // Test default sitemap.
    $this->drupalGet($this->defaultSitemapUrl);
    $this->assertSession()->responseContains('node/' . $this->node->id());
    $this->assertSession()->responseContains('node/' . $this->node2->id());
    $this->assertSession()->responseNotContains('media/1');

    // Test auth sitemap.
    $this->drupalGet($this->authSitemapUrl);
    $this->assertSession()->responseContains('node/' . $this->node->id());
    $this->assertSession()->responseContains('node/' . $this->node2->id());
    $this->assertSession()->responseContains('media/1');
  }

}
