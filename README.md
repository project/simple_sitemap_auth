CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The Simple Sitemap Auth module allows users to provide a sitemap with nodes
which are only accessible for an authenticated user.


 * For a full description of the module visit:
   https://www.drupal.org/project/simple_sitemap_auth

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/simple_sitemap_auth


REQUIREMENTS
------------

This module requires the Simple Sitemap module.


INSTALLATION
------------

Install the Simple Sitemap Auth module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Simple Sitemap Auth
       module.
    2. Navigate to Administration > Configuration > Search and metadata >
       Simple XML Sitemap > User and choose the user.
    3. Navigate to Administration > Configuration > Search and metadata >
       Simple XML Sitemap > Sitemaps > Variants and add/change a variant using
       the auth_hreflang sitemap type and generate the sitemap.


TROUBLESHOOTING
---------------


FAQ
---


MAINTAINERS
-----------

 * sleitner - https://www.drupal.org/u/sleitner
