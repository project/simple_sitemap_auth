<?php

namespace Drupal\simple_sitemap_auth\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_sitemap\Form\SimpleSitemapFormBase;
use Drupal\user\Entity\User;

/**
 * SimplesitemapForm for Auth.
 *
 * @package Drupal\simple_sitemap_auth\Form
 */
class SimplesitemapauthSettingsForm extends SimpleSitemapFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_sitemap_auth_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return ['config.simple_sitemap_auth'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('simple_sitemap_auth.settings');
    $form['simple_sitemap_settings']['user'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('User'),
      '#description' => $this->t('The user whose permissions are used for crawling.'),
      '#target_type' => 'user',
      '#default_value' => User::load($config->get('user')),
      '#required' => TRUE,
      '#selection_settings' => [
        'include_anonymous' => TRUE,
        'filter' => [
          'type' => '_none',
        ],
      ],
    ];
    $form = $this->formHelper->regenerateNowForm($form);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('simple_sitemap_auth.settings');
    $config->set('user', $form_state->getValue('user'));
    $config->save();

    parent::submitForm($form, $form_state);

    // Regenerate sitemaps according to user setting.
    $this->formHelper->regenerateNowFormSubmit($form, $form_state);
  }

}
